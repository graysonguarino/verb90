/*
  ==============================================================================

    APLoop.h
    Created: 2 Oct 2021 11:27:58am
    Author:  Grayson Guarino

  ==============================================================================
*/

#pragma once
#include <JuceHeader.h>
#include "AP.h"
#include "Delay.h"

class APLoop {
public:
    APLoop();
    
    AP ap1;
    AP ap2;
    
    AP ap3;
    AP ap4;
    
    AP ap5;
    AP ap6;
    
    AP ap7;
    AP ap8;
    
    Delay d1 {44100 * 5};
    Delay d2 {44100 * 5};
    Delay d3 {44100 * 5};
    Delay d4 {44100 * 5};
    
    void prepare (juce::dsp::ProcessSpec spec, float coeff);
    
    void updateCoeffVal (juce::SmoothedValue<float, juce::ValueSmoothingTypes::Linear> coeffValSmooth);
    void updateDelayVal (float delayAmt);
};


