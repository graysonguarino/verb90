/*
  ==============================================================================

    Reverb.h
    Created: 14 Sep 2021 4:41:44pm
    Author:  Grayson Guarino

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>
#include "EarlyReflections.h"
#include "APLoop.h"

class Reverb
{
public:
    Reverb();
    
    Reverb (juce::dsp::ProcessSpec spec);
    
    /** To be called before process() is called. This is called in the constructor.
    */
    void prepare (juce::dsp::ProcessSpec spec);
    
    /** Clears out the reverb when playback is restarted from a stopped state.
    */
    void reset();
    
    /** Processes the AudioBlock with the reverb algorithm.
    */
    void process (juce::dsp::AudioBlock<float>& block);
    
    /** Called by Verb90AudioProcessor::parameterChanged() to change the all-pass coefficient value.
    */
    void setCoeffVal (float coeff);
    
    /** Called by Verb90AudioProcessor::parameterChanged() to change the early .
    */
    void setEarlyMix (float mix) {mEarlyMixAmtSmooth.setTargetValue (mix);}
    
    /** Called by Verb90AudioProcessor::parameterChanged() to change the total mix amount value.
    */
    void setTotMixAmt (float mix) {mTotMixAmtSmooth.setTargetValue (mix);}
    
    /** Called by Verb90AudioProcessor::parameterChanged() to change set the target value for the smoothed early delay amount value.
    */
    void setEarlyDelayAmt (float delayAmt);
    
    /** Called by Verb90AudioProcessor::parameterChanged() to change set the target value for the smoothed all-pass loop delay amount value.
    */
    void setLoopDelayAmt (float delayAmt);
    
    /** Called by Verb90AudioProcessor::parameterChanged() to change the value of mLowPassFreq which is used in applyLowPass()
    */
    void setLowPassFreq (int freq) {jassert (freq <= 20000); mLowPassFreq = freq;}
    
    /** Called by Verb90AudioProcessor::parameterChanged() to change the value of mHighPassFreq which is used in applyHighPass()
    */
    void setHighPassFreq (int freq) {jassert (freq >= 0); mHighPassFreq = freq;}
    
private:
    juce::dsp::ProcessSpec mSpec;
    
    EarlyReflections leftEarlyReflections;
    EarlyReflections rightEarlyReflections;
    
    APLoop apLoop;

    
    void processMono (juce::dsp::AudioBlock<float>& block);
    void processStereo (juce::dsp::AudioBlock<float>& block);
    
    void applyLowPass (juce::dsp::AudioBlock<float>& block);
    void applyHighPass (juce::dsp::AudioBlock<float>& block);
    
    juce::SmoothedValue<float, juce::ValueSmoothingTypes::Linear> mCoeffValSmooth;
    float mEarlyDelayAmt;
    float mLoopDelayAmt;
    juce::SmoothedValue<float, juce::ValueSmoothingTypes::Linear> mEarlyMixAmtSmooth;
    juce::SmoothedValue<float, juce::ValueSmoothingTypes::Linear> mTotMixAmtSmooth;
    int mLowPassFreq = 20000;
    int mHighPassFreq = 1;
    
    juce::dsp::ProcessorDuplicator<juce::dsp::IIR::Filter<float>, juce::dsp::IIR::Coefficients<float>> mLowPassFilter;
    juce::dsp::ProcessorDuplicator<juce::dsp::IIR::Filter<float>, juce::dsp::IIR::Coefficients<float>> mHighPassFilter;
};

