/*
  ==============================================================================

    Reverb.cpp
    Created: 14 Sep 2021 4:41:44pm
    Author:  Grayson Guarino

  ==============================================================================
*/

#include "Reverb.h"


Reverb::Reverb()
{
}

Reverb::Reverb (juce::dsp::ProcessSpec spec)
{
    prepare (spec);
}

void Reverb::prepare (juce::dsp::ProcessSpec spec)
{
    mSpec = spec;
    
    leftEarlyReflections.prepare (mSpec, mCoeffValSmooth.getTargetValue(), true);
    rightEarlyReflections.prepare (mSpec, mCoeffValSmooth.getTargetValue(), false);
    apLoop.prepare (mSpec, mCoeffValSmooth.getTargetValue());
    
    mLowPassFilter.prepare (mSpec);
    mLowPassFilter.reset();
    
    mHighPassFilter.prepare (mSpec);
    mLowPassFilter.reset();
}

void Reverb::reset()
{
    apLoop.d1.reset();
    apLoop.d2.reset();
    apLoop.d3.reset();
    apLoop.d4.reset();
    
    apLoop.updateDelayVal (mLoopDelayAmt);
}

void Reverb::process (juce::dsp::AudioBlock<float>& block)
{
    applyLowPass (block);
    applyHighPass (block);
    
    bool isStereo = false;
    if (mSpec.numChannels == 2) {
        isStereo = true;
    }
    
    if (isStereo) {
        processStereo (block);
    } else {
        processMono (block);
    }
}

void Reverb::processMono (juce::dsp::AudioBlock<float>& block)
{
    auto bufferData = block.getChannelPointer (0);
    
    for (int samplePos = 0; samplePos < block.getNumSamples(); samplePos++) {
        
        float lap1Out = leftEarlyReflections.ap1.process (bufferData[samplePos]);
        float lap2Out = leftEarlyReflections.ap2.process (lap1Out);
        float lap3Out = leftEarlyReflections.ap3.process (lap2Out);
        float lap4Out = leftEarlyReflections.ap4.process (lap3Out);
        
        float ap1Out = apLoop.ap1.process (lap4Out);
        float ap2Out = apLoop.ap2.process (ap1Out);
        apLoop.d1.pushSample (0, ap2Out);
        float delayed1 = apLoop.d1.popSample();
        
        float loopOut1 = delayed1 + ap2Out;
        
        float ap3Out = apLoop.ap3.process (loopOut1);
        float ap4Out = apLoop.ap4.process (ap3Out);
        apLoop.d2.pushSample (0, ap4Out);
        float delayed2 = apLoop.d2.popSample();
        
        float loopOut2 = delayed2 + ap4Out;
        
        float ap5Out = apLoop.ap5.process (loopOut2);
        float ap6Out = apLoop.ap6.process (ap5Out);
        apLoop.d3.pushSample (0, ap6Out);
        float delayed3 = apLoop.d3.popSample();
        
        float loopOut3 = delayed3 + ap6Out;
        
        float ap7Out = apLoop.ap7.process (loopOut3);
        float ap8Out = apLoop.ap8.process (ap7Out);
        apLoop.d4.pushSample (0, ap8Out);
        float delayed4 = apLoop.d4.popSample();
        
        float loopOut4 = delayed4 + ap8Out;
        
        // Write processed signal back into buffer
        float writeBack = (loopOut1 * 0.25) + (loopOut2 * 0.25) + (loopOut3 * 0.25) + (loopOut4 * 0.25);
        
        block.addSample (0, samplePos, writeBack);
    }
}

void Reverb::processStereo (juce::dsp::AudioBlock<float>& block)
{
    auto bufferDataL = block.getChannelPointer (0);
    auto bufferDataR = block.getChannelPointer (1);
    
    for (int samplePos = 0; samplePos < block.getNumSamples(); samplePos++) {
        
        float lap1Out = leftEarlyReflections.ap1.process (bufferDataL[samplePos]);
        float lap2Out = leftEarlyReflections.ap2.process (lap1Out);
        float lap3Out = leftEarlyReflections.ap3.process (lap2Out);
        float lap4Out = leftEarlyReflections.ap4.process (lap3Out);
        
        float rap1Out = rightEarlyReflections.ap1.process (bufferDataR[samplePos]);
        float rap2Out = rightEarlyReflections.ap2.process (rap1Out);
        float rap3Out = rightEarlyReflections.ap3.process (rap2Out);
        float rap4Out = rightEarlyReflections.ap4.process (rap3Out);
        
        float ap1Out = apLoop.ap1.process (lap4Out);
        float ap2Out = apLoop.ap2.process (ap1Out);
        apLoop.d1.pushSample (0, ap2Out);
        float delayed1 = apLoop.d1.popSample();
        
        float loopOut1 = delayed1 + ap2Out;
        float loopIn2 = loopOut1 + (rap4Out * 0.5);
        
        float ap3Out = apLoop.ap3.process (loopIn2);
        float ap4Out = apLoop.ap4.process (ap3Out);
        apLoop.d2.pushSample (0, ap4Out);
        float delayed2 = apLoop.d2.popSample();
        
        float loopOut2 = delayed2 + ap4Out;
        float loopIn3 = loopOut2 + (lap4Out * 0.5);
        
        float ap5Out = apLoop.ap5.process (loopIn3);
        float ap6Out = apLoop.ap6.process (ap5Out);
        apLoop.d3.pushSample (0, ap6Out);
        float delayed3 = apLoop.d3.popSample();
        
        float loopOut3 = delayed3 + ap6Out;
        float loopIn4 = loopOut3 + (rap4Out * 0.5);
        
        float ap7Out = apLoop.ap7.process (loopIn4);
        float ap8Out = apLoop.ap8.process (ap7Out);
        apLoop.d4.pushSample (0, ap8Out);
        float delayed4 = apLoop.d4.popSample();
        
        float loopOut4 = delayed4 + ap8Out;
        
        // Write processed signal back into buffer
        float writeBackL = loopOut3;
        float writeBackR = loopOut4;
        
        block.addSample (0, samplePos, writeBackL);
        block.addSample (1, samplePos, writeBackR);
    }
}

void Reverb::applyLowPass(juce::dsp::AudioBlock<float>& block)
{
    *mLowPassFilter.state = *juce::dsp::IIR::Coefficients<float>::makeLowPass(mSpec.sampleRate, mLowPassFreq);
    mLowPassFilter.process (juce::dsp::ProcessContextReplacing<float> (block));
}

void Reverb::applyHighPass(juce::dsp::AudioBlock<float>& block)
{
    *mHighPassFilter.state = *juce::dsp::IIR::Coefficients<float>::makeHighPass(mSpec.sampleRate, mHighPassFreq);
    mHighPassFilter.process (juce::dsp::ProcessContextReplacing<float> (block));
}

void Reverb::setCoeffVal (float coeff)
{
    mCoeffValSmooth.setTargetValue (coeff);
    leftEarlyReflections.updateCoeffVal (mCoeffValSmooth);
    rightEarlyReflections.updateCoeffVal (mCoeffValSmooth);
    apLoop.updateCoeffVal (mCoeffValSmooth);
}

void Reverb::setEarlyDelayAmt (float delayAmt)
{
    mEarlyDelayAmt = delayAmt;
}

void Reverb::setLoopDelayAmt (float delayAmt)
{
    mLoopDelayAmt = delayAmt;
    apLoop.updateDelayVal (mLoopDelayAmt);
}

