/*
  ==============================================================================

    This file contains the basic framework code for a JUCE plugin processor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"


//==============================================================================
Verb90AudioProcessor::Verb90AudioProcessor()
#ifndef JucePlugin_PreferredChannelConfigurations
     : AudioProcessor (BusesProperties()
                     #if ! JucePlugin_IsMidiEffect
                      #if ! JucePlugin_IsSynth
                       .withInput  ("Input",  juce::AudioChannelSet::stereo(), true)
                      #endif
                       .withOutput ("Output", juce::AudioChannelSet::stereo(), true)
                     #endif
                       ), apvts (*this, nullptr, "Parameters", createParameters())
#endif
{
    apvts.addParameterListener ("COEFF", this);
    apvts.addParameterListener ("EARLY_DELAY", this);
    apvts.addParameterListener ("LOOP_DELAY", this);
    apvts.addParameterListener ("EARLY_MIX", this);
    apvts.addParameterListener ("HIGH_PASS", this);
    apvts.addParameterListener ("LOW_PASS", this);
    apvts.addParameterListener ("MIX", this);
}

Verb90AudioProcessor::~Verb90AudioProcessor()
{
    apvts.removeParameterListener ("COEFF", this);
    apvts.removeParameterListener ("EARLY_DELAY", this);
    apvts.removeParameterListener ("LOOP_DELAY", this);
    apvts.removeParameterListener ("EARLY_MIX", this);
    apvts.removeParameterListener ("HIGH_PASS", this);
    apvts.removeParameterListener ("LOW_PASS", this);
    apvts.removeParameterListener ("MIX", this);
}

//==============================================================================
const juce::String Verb90AudioProcessor::getName() const
{
    return JucePlugin_Name;
}

bool Verb90AudioProcessor::acceptsMidi() const
{
   #if JucePlugin_WantsMidiInput
    return true;
   #else
    return false;
   #endif
}

bool Verb90AudioProcessor::producesMidi() const
{
   #if JucePlugin_ProducesMidiOutput
    return true;
   #else
    return false;
   #endif
}

bool Verb90AudioProcessor::isMidiEffect() const
{
   #if JucePlugin_IsMidiEffect
    return true;
   #else
    return false;
   #endif
}

double Verb90AudioProcessor::getTailLengthSeconds() const
{
    return 0.0;
}

int Verb90AudioProcessor::getNumPrograms()
{
    return 1;   // NB: some hosts don't cope very well if you tell them there are 0 programs,
                // so this should be at least 1, even if you're not really implementing programs.
}

int Verb90AudioProcessor::getCurrentProgram()
{
    return 0;
}

void Verb90AudioProcessor::setCurrentProgram (int index)
{
}

const juce::String Verb90AudioProcessor::getProgramName (int index)
{
    return {};
}

void Verb90AudioProcessor::changeProgramName (int index, const juce::String& newName)
{
}

//==============================================================================
void Verb90AudioProcessor::prepareToPlay (double sampleRate, int samplesPerBlock)
{
    mSpec.sampleRate = sampleRate;
    mSpec.numChannels = getTotalNumOutputChannels();
    mSpec.maximumBlockSize = samplesPerBlock;
    
    reverb.setCoeffVal (*apvts.getRawParameterValue ("COEFF"));
    reverb.setEarlyDelayAmt (*apvts.getRawParameterValue ("EARLY_DELAY"));
    reverb.setLoopDelayAmt (*apvts.getRawParameterValue ("LOOP_DELAY"));
    reverb.setEarlyMix (*apvts.getRawParameterValue ("EARLY_MIX"));
    reverb.setHighPassFreq (*apvts.getRawParameterValue ("HIGH_PASS"));
    reverb.setLowPassFreq (*apvts.getRawParameterValue ("LOW_PASS"));
    reverb.setTotMixAmt (*apvts.getRawParameterValue ("MIX"));
    
    reverb.prepare (mSpec);
    reverb.reset();
    
    limiter.prepare (mSpec);
    limiter.reset();
    limiter.setThreshold (0.f);
    limiter.setRelease (250.f);
}

void Verb90AudioProcessor::releaseResources()
{
    // When playback stops, you can use this as an opportunity to free up any
    // spare memory, etc.
}

#ifndef JucePlugin_PreferredChannelConfigurations
bool Verb90AudioProcessor::isBusesLayoutSupported (const BusesLayout& layouts) const
{
  #if JucePlugin_IsMidiEffect
    juce::ignoreUnused (layouts);
    return true;
  #else
    // This is the place where you check if the layout is supported.
    // In this template code we only support mono or stereo.
    // Some plugin hosts, such as certain GarageBand versions, will only
    // load plugins that support stereo bus layouts.
    if (layouts.getMainOutputChannelSet() != juce::AudioChannelSet::mono()
     && layouts.getMainOutputChannelSet() != juce::AudioChannelSet::stereo())
        return false;

    // This checks if the input layout matches the output layout
   #if ! JucePlugin_IsSynth
    if (layouts.getMainOutputChannelSet() != layouts.getMainInputChannelSet())
        return false;
   #endif

    return true;
  #endif
}
#endif

void Verb90AudioProcessor::processBlock (juce::AudioBuffer<float>& buffer, juce::MidiBuffer& midiMessages)
{
    juce::ScopedNoDenormals noDenormals;
    auto totalNumInputChannels  = getTotalNumInputChannels();
    auto totalNumOutputChannels = getTotalNumOutputChannels();

    // In case we have more outputs than inputs, this code clears any output
    // channels that didn't contain input data, (because these aren't
    // guaranteed to be empty - they may contain garbage).
    // This is here to avoid people getting screaming feedback
    // when they first compile a plugin, but obviously you don't need to keep
    // this code if your algorithm always overwrites all the output channels.
    for (auto i = totalNumInputChannels; i < totalNumOutputChannels; ++i)
        buffer.clear (i, 0, buffer.getNumSamples());
    
    juce::dsp::AudioBlock<float> block (buffer);
    reverb.process (block);
    limiter.process (juce::dsp::ProcessContextReplacing<float> (block));
}

//==============================================================================
bool Verb90AudioProcessor::hasEditor() const
{
    return true; // (change this to false if you choose to not supply an editor)
}

juce::AudioProcessorEditor* Verb90AudioProcessor::createEditor()
{
    return new Verb90AudioProcessorEditor (*this);
}

//==============================================================================
void Verb90AudioProcessor::getStateInformation (juce::MemoryBlock& destData)
{
    auto state = apvts.copyState();
    std::unique_ptr<juce::XmlElement> xml (state.createXml());
    copyXmlToBinary (*xml, destData);
}

void Verb90AudioProcessor::setStateInformation (const void* data, int sizeInBytes)
{
    std::unique_ptr<juce::XmlElement> xmlState (getXmlFromBinary (data, sizeInBytes));

    if (xmlState.get() != nullptr)
        if (xmlState->hasTagName (apvts.state.getType()))
            apvts.replaceState (juce::ValueTree::fromXml (*xmlState));
}

void Verb90AudioProcessor::parameterChanged (const juce::String& parameterID, float newValue) {
    
    if (parameterID == "COEFF")
        reverb.setCoeffVal (newValue);

    if (parameterID == "EARLY_DELAY")
        reverb.setEarlyDelayAmt (newValue);

    if (parameterID == "LOOP_DELAY")
        reverb.setLoopDelayAmt (newValue);

    if (parameterID == "EARLY_MIX")
        reverb.setEarlyMix (newValue);
    
    if (parameterID == "HIGH_PASS")
        reverb.setHighPassFreq (newValue);
    
    if (parameterID == "LOW_PASS")
        reverb.setLowPassFreq (newValue);

    if (parameterID == "MIX")
        reverb.setTotMixAmt (newValue);

}

juce::AudioProcessorValueTreeState::ParameterLayout
    Verb90AudioProcessor::createParameters()
{
        juce::AudioProcessorValueTreeState::ParameterLayout params;
        
        params.add(std::make_unique<juce::AudioParameterFloat> ("COEFF", // ID
                                                               "Coeff", // name
                                                               juce::NormalisableRange<float> (0.f, 0.8f, 0.05f), // rangeStart, rangeEnd, intervalValue
                                                               0.618f)); // default value
        
        params.add(std::make_unique<juce::AudioParameterFloat> ("EARLY_DELAY", // ID
                                                               "Early Delay", // name
                                                               juce::NormalisableRange<float> (0.01f, 1.f, 0.01f), // rangeStart, rangeEnd, intervalValue
                                                               0.5f)); // default value

        params.add(std::make_unique<juce::AudioParameterFloat> ("LOOP_DELAY", // ID
                                                               "Loop Delay", // name
                                                               juce::NormalisableRange<float> (0.5f, 1.5f, 0.01f), // rangeStart, rangeEnd, intervalValue
                                                               1.f)); // default value

        params.add(std::make_unique<juce::AudioParameterFloat> ("EARLY_MIX",
                                                               "Feedback",
                                                               juce::NormalisableRange<float> (0.f, 1.f, 0.01f), // rangeStart, rangeEnd, intervalValue
                                                               .5f));
        
        params.add(std::make_unique<juce::AudioParameterFloat> ("HIGH_PASS",
                                                                "High Pass",
                                                                juce::NormalisableRange<float> (20.f, 20000.f, 1.f), // rangeStart, rangeEnd, intervalValue
                                                                20.f));
        
        params.add(std::make_unique<juce::AudioParameterFloat> ("LOW_PASS",
                                                                "Low Pass",
                                                                juce::NormalisableRange<float> (20.f, 20000.f, 1.f), // rangeStart, rangeEnd, intervalValue
                                                                20000.f));

        params.add(std::make_unique<juce::AudioParameterFloat> ("MIX",
                                                               "Mix",
                                                               juce::NormalisableRange<float> (0.f, 1.f, 0.01f), // rangeStart, rangeEnd, intervalValue
                                                               .5f));
                                   
                                   
        return params;
        
}

//==============================================================================
// This creates new instances of the plugin..
juce::AudioProcessor* JUCE_CALLTYPE createPluginFilter()
{
    return new Verb90AudioProcessor();
}
