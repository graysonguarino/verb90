/*
  ==============================================================================

    Delay.cpp
    Created: 13 Oct 2021 1:17:17pm
    Author:  Grayson Guarino

  ==============================================================================
*/

#include "Delay.h"

Delay::Delay (int maximumDelayInSamples)
: juce::dsp::DelayLine<float, juce::dsp::DelayLineInterpolationTypes::Linear> {44100 * 5}
{
}

void Delay::setInitialDelayAmt (int delayAmt)
{
    initialDelayAmt = delayAmt;
    delayAmtSmooth.reset (5000);
    setDelay (delayAmt);
}

int Delay::getInitialDelayAmt()
{
    return initialDelayAmt;
}

void Delay::setDelay (int delayAmt)
{
    jassert (delayAmt >= 0);
    delayAmtSmooth.setTargetValue (delayAmt);
    juce::dsp::DelayLine<float, juce::dsp::DelayLineInterpolationTypes::Linear>::setDelay (delayAmtSmooth.getNextValue());
}

float Delay::popSample ()
{
    juce::dsp::DelayLine<float, juce::dsp::DelayLineInterpolationTypes::Linear>::setDelay (delayAmtSmooth.getNextValue());
    return juce::dsp::DelayLine<float, juce::dsp::DelayLineInterpolationTypes::Linear>::popSample(0);
}
