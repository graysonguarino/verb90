/*
  ==============================================================================

    AP.h
    Created: 14 Sep 2021 4:41:34pm
    Author:  Grayson Guarino

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>
#include "Delay.h"

class AP : public Delay
{
public:
    AP();
    
    /** Basic constructor which only takes in a ProcessSpec. prepare() must be called with coeff and delayAmt if this constructor is used.
    */
    AP (juce::dsp::ProcessSpec spec);
    
    /** Preferred constructor. prepare() does not need to be called if this constructor is used.
    */
    AP (juce::dsp::ProcessSpec spec, float coeff, int delayAmt);
    
    void setCoeff (float coeff) {jassert (coeff > -1); mCoeff = coeff;}

    float getCoeff() {return mCoeff;}
    
    bool isCoeffReversed() {if (mCoeff <= 0) return true; else return false;}
    
    /** To be called before process is called. This is called in the constructor if given coeff and delayAmt.
    */
    void prepare (juce::dsp::ProcessSpec spec, float coeff, int delayAmt);
    
    /** Performs allpass on the inputted sample of type float, returns the summed feedforward and delayed sample pop.
    */
    float process (float sample);
    
    /** Sets the coefficient to the negative version of itself for diversity in an allpass loop.
    */
    void reverseCoeff() {mCoeff = mCoeff * -1;}
    
private:
    float mCoeff = -2;
    
    juce::dsp::ProcessSpec mSpec;
};
