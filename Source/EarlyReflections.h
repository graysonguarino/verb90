/*
  ==============================================================================

    EarlyReflection.h
    Created: 2 Oct 2021 11:27:44am
    Author:  Grayson Guarino

  ==============================================================================
*/

#pragma once
#include <JuceHeader.h>
#include "AP.h"

class EarlyReflections
{
public:
    EarlyReflections();
    
    bool isLeft;
    
    AP ap1;
    AP ap2;
    AP ap3;
    AP ap4;
    
    void prepare (juce::dsp::ProcessSpec spec, float coeff, bool isLeft);
    void updateCoeffVal (juce::SmoothedValue<float, juce::ValueSmoothingTypes::Linear> coeffValSmooth);
    
};
