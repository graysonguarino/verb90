/*
  ==============================================================================

    EarlyReflection.cpp
    Created: 2 Oct 2021 11:27:44am
    Author:  Grayson Guarino

  ==============================================================================
*/

#include "EarlyReflections.h"

EarlyReflections::EarlyReflections()
{
}

void EarlyReflections::prepare (juce::dsp::ProcessSpec spec, float coeff, bool isLeft) {
    int delay1; // Values for delay times of allpasses. Changes slightly based on channel.
    int delay2;
    int delay3;
    int delay4;
    
    if (isLeft) {
        delay1 = 156;
        delay2 = 223;
        delay3 = 332;
        delay4 = 548;
    } else {
        delay1 = 186;
        delay2 = 253;
        delay3 = 302;
        delay4 = 498;
    }
    
    ap1.prepare (spec, coeff, delay1);
    ap2.prepare (spec, coeff, delay2);
    ap3.prepare (spec, coeff, delay3);
    ap4.prepare (spec, coeff, delay4);
}

void EarlyReflections::updateCoeffVal (juce::SmoothedValue<float, juce::ValueSmoothingTypes::Linear> coeffValSmooth)
{
    float newVal = coeffValSmooth.getNextValue();
    ap1.setCoeff (newVal);
    ap2.setCoeff (newVal);
    ap3.setCoeff (newVal);
    ap4.setCoeff (newVal);
}
