/*
  ==============================================================================

    This file contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>
#include "PluginProcessor.h"

//==============================================================================
/**
*/
class Verb90AudioProcessorEditor  : public juce::AudioProcessorEditor
{
public:
    Verb90AudioProcessorEditor (Verb90AudioProcessor&);
    ~Verb90AudioProcessorEditor() override;

    //==============================================================================
    void paint (juce::Graphics&) override;
    void resized() override;

private:
    // This reference is provided as a quick way for your editor to
    // access the processor object that created it.
    Verb90AudioProcessor& audioProcessor;
    
    juce::Slider coeffSlider;
    juce::Slider earlyDelaySlider;
    juce::Slider loopDelaySlider;
    juce::Slider earlyMixSlider;
    juce::Slider highPassFreqSlider;
    juce::Slider lowPassFreqSlider;
    juce::Slider mixSlider;
    
    std::unique_ptr<juce::AudioProcessorValueTreeState::SliderAttachment> coeffSliderAttachment;
    std::unique_ptr<juce::AudioProcessorValueTreeState::SliderAttachment> earlyDelaySliderAttachment;
    std::unique_ptr<juce::AudioProcessorValueTreeState::SliderAttachment> loopDelaySliderAttachment;
    std::unique_ptr<juce::AudioProcessorValueTreeState::SliderAttachment> earlyMixSliderAttachment;
    std::unique_ptr<juce::AudioProcessorValueTreeState::SliderAttachment> highPassFreqSliderAttachment;
    std::unique_ptr<juce::AudioProcessorValueTreeState::SliderAttachment> lowPassFreqSliderAttachment;
    std::unique_ptr<juce::AudioProcessorValueTreeState::SliderAttachment> mixSliderAttachment;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (Verb90AudioProcessorEditor)
};
