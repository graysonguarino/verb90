/*
  ==============================================================================

    This file contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"

//==============================================================================
Verb90AudioProcessorEditor::Verb90AudioProcessorEditor (Verb90AudioProcessor& p)
    : AudioProcessorEditor (&p), audioProcessor (p)
{
    // Make sure that before the constructor has finished, you've set the
    // editor's size to whatever you need it to be.
    setSize (1000, 500);
    
    coeffSlider.setSliderStyle (juce::Slider::SliderStyle::RotaryHorizontalVerticalDrag);
    coeffSlider.setTextBoxStyle (juce::Slider::TextBoxBelow, true, 100, 50);
    addAndMakeVisible (coeffSlider);
    
    earlyDelaySlider.setSliderStyle (juce::Slider::SliderStyle::RotaryHorizontalVerticalDrag);
    earlyDelaySlider.setTextBoxStyle (juce::Slider::TextBoxBelow, true, 100, 50);
    addAndMakeVisible (earlyDelaySlider);
    
    loopDelaySlider.setSliderStyle (juce::Slider::SliderStyle::RotaryHorizontalVerticalDrag);
    loopDelaySlider.setTextBoxStyle (juce::Slider::TextBoxBelow, true, 100, 50);
    addAndMakeVisible (loopDelaySlider);
    
    earlyMixSlider.setSliderStyle (juce::Slider::SliderStyle::RotaryHorizontalVerticalDrag);
    earlyMixSlider.setTextBoxStyle (juce::Slider::TextBoxBelow, true, 100, 50);
    addAndMakeVisible (earlyMixSlider);
    
    highPassFreqSlider.setSliderStyle (juce::Slider::SliderStyle::RotaryHorizontalVerticalDrag);
    highPassFreqSlider.setTextBoxStyle (juce::Slider::TextBoxBelow, true, 100, 50);
    addAndMakeVisible (highPassFreqSlider);
    
    lowPassFreqSlider.setSliderStyle (juce::Slider::SliderStyle::RotaryHorizontalVerticalDrag);
    lowPassFreqSlider.setTextBoxStyle (juce::Slider::TextBoxBelow, true, 100, 50);
    addAndMakeVisible (lowPassFreqSlider);
    
    mixSlider.setSliderStyle (juce::Slider::SliderStyle::RotaryHorizontalVerticalDrag);
    mixSlider.setTextBoxStyle (juce::Slider::TextBoxBelow, true, 100, 50);
    addAndMakeVisible (mixSlider);
    
    coeffSliderAttachment = std::make_unique<juce::AudioProcessorValueTreeState::SliderAttachment> (audioProcessor.apvts, "COEFF", coeffSlider);
    earlyDelaySliderAttachment = std::make_unique<juce::AudioProcessorValueTreeState::SliderAttachment> (audioProcessor.apvts, "EARLY_DELAY", earlyDelaySlider);
    loopDelaySliderAttachment = std::make_unique<juce::AudioProcessorValueTreeState::SliderAttachment> (audioProcessor.apvts, "LOOP_DELAY", loopDelaySlider);
    earlyMixSliderAttachment = std::make_unique<juce::AudioProcessorValueTreeState::SliderAttachment> (audioProcessor.apvts, "EARLY_MIX", earlyMixSlider);
    highPassFreqSliderAttachment = std::make_unique<juce::AudioProcessorValueTreeState::SliderAttachment> (audioProcessor.apvts, "HIGH_PASS", highPassFreqSlider);
    lowPassFreqSliderAttachment = std::make_unique<juce::AudioProcessorValueTreeState::SliderAttachment> (audioProcessor.apvts, "LOW_PASS", lowPassFreqSlider);
    mixSliderAttachment = std::make_unique<juce::AudioProcessorValueTreeState::SliderAttachment> (audioProcessor.apvts, "MIX", mixSlider);
}

Verb90AudioProcessorEditor::~Verb90AudioProcessorEditor()
{
}

//==============================================================================
void Verb90AudioProcessorEditor::paint (juce::Graphics& g)
{
    // (Our component is opaque, so we must completely fill the background with a solid colour)
    g.fillAll (getLookAndFeel().findColour (juce::ResizableWindow::backgroundColourId));
}

void Verb90AudioProcessorEditor::resized()
{
    coeffSlider.setBounds (0, getHeight() / 2 - 50, 200, 100);
    earlyDelaySlider.setBounds (coeffSlider.getX() + 125, coeffSlider.getY(), 200, 100);
    loopDelaySlider.setBounds(earlyDelaySlider.getX() + 125, earlyDelaySlider.getY(), 200, 100);
    earlyMixSlider.setBounds (loopDelaySlider.getX() + 125, loopDelaySlider.getY(), 200, 100);
    highPassFreqSlider.setBounds (earlyMixSlider.getX() + 125, earlyMixSlider.getY(), 200, 100);
    lowPassFreqSlider.setBounds (highPassFreqSlider.getX() + 125, highPassFreqSlider.getY(), 200, 100);
    mixSlider.setBounds (lowPassFreqSlider.getX() + 125, lowPassFreqSlider.getY(), 200, 100);
}
