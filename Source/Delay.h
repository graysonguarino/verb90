/*
  ==============================================================================

    Delay.h
    Created: 13 Oct 2021 1:17:17pm
    Author:  Grayson Guarino

  ==============================================================================
*/

#pragma once
#include <JuceHeader.h>

class Delay : public juce::dsp::DelayLine<float, juce::dsp::DelayLineInterpolationTypes::Linear> {
public:
    Delay (int maximumDelayInSamples);
    
    void setInitialDelayAmt (int delayAmt);
    int getInitialDelayAmt();
    
    void setDelay (int delayAmt);
    
    float popSample();
    
    int getCurrentDelayTarget() {return delayAmtSmooth.getTargetValue();}
    
    juce::SmoothedValue<float, juce::ValueSmoothingTypes::Linear> delayAmtSmooth;
    
private:
    /** Holds the value of the initial delay which is modified with a multiplier to get an actual delay time.
    */
    float initialDelayAmt;
};
