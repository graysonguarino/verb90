/*
  ==============================================================================

    AP.cpp
    Created: 14 Sep 2021 4:41:34pm
    Author:  Grayson Guarino

  ==============================================================================
*/

#include "AP.h"

AP::AP()
: Delay {44100 * 5}
{
    
}

AP::AP (juce::dsp::ProcessSpec spec)
: Delay {44100 * 5}
{
    mSpec = spec;
    mSpec.numChannels = 1;
}

AP::AP (juce::dsp::ProcessSpec spec, float coeff, int delayAmt)
: Delay {44100 * 5}
{
    prepare (spec, coeff, delayAmt);
}

void AP::prepare (juce::dsp::ProcessSpec spec, float coeff, int delayAmt)
{
    jassert (delayAmt >= 0);

    mSpec = spec;
    mSpec.numChannels = 1;
    juce::dsp::DelayLine<float>::prepare (mSpec);
    
    setCoeff (coeff);
    setInitialDelayAmt (delayAmt);
    reset();

    delayAmtSmooth.reset (5000);
    delayAmtSmooth.setCurrentAndTargetValue (delayAmt);
}

float AP::process (float sample)
{
    setDelay (delayAmtSmooth.getNextValue());
    float delayedSample = popSample();
    float feedbackSample = delayedSample * mCoeff;
    
    // Sum incoming sample with feedback
    sample += feedbackSample;
    
    // Feedforward
    float feedforwardSample = sample * (-1 * mCoeff);
    
    // Push sample to delay
    pushSample (0, sample);
    
    // Output summing
    return delayedSample + feedforwardSample;
}
