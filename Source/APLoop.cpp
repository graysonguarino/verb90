/*
  ==============================================================================

    APLoop.cpp
    Created: 2 Oct 2021 11:27:58am
    Author:  Grayson Guarino

  ==============================================================================
*/

#include "APLoop.h"

APLoop::APLoop()
{
    
}

void APLoop::prepare (juce::dsp::ProcessSpec spec, float coeff)
{
    ap1.prepare (spec, coeff, 1251);
    ap2.prepare (spec, coeff, 1751);
    d1.prepare (spec);
    d1.reset();
    d1.setInitialDelayAmt (5859);
    
    ap3.prepare (spec, coeff, 1443);
    ap4.prepare (spec, coeff, 1343);
    d2.prepare (spec);
    d2.reset();
    d2.setInitialDelayAmt (4145);
    
    ap5.prepare (spec, coeff, 1582);
    ap6.prepare (spec, coeff, 1981);
    d3.prepare (spec);
    d3.reset();
    d3.setInitialDelayAmt (3476);
    
    ap7.prepare (spec, coeff, 1274);
    ap8.prepare (spec, coeff, 1382);
    d4.prepare (spec);
    d4.reset();
    d4.setInitialDelayAmt (4568);
}

void APLoop::updateCoeffVal (juce::SmoothedValue<float, juce::ValueSmoothingTypes::Linear> coeffValSmooth)
{
    float newVal = coeffValSmooth.getNextValue();
    ap1.setCoeff (newVal);
    ap2.setCoeff (newVal);
    ap3.setCoeff (newVal);
    ap4.setCoeff (newVal);
    ap5.setCoeff (newVal);
    ap6.setCoeff (newVal);
    ap7.setCoeff (newVal);
    ap8.setCoeff (newVal);
}

void APLoop::updateDelayVal (float delayMultiplier)
{
    // TODO - fix bug where this is triggered by the default Delay.mDelayAmt = -1
    jassert (delayMultiplier > 0);
    
    int d1NewDelay = d1.getInitialDelayAmt() * delayMultiplier;
    int d2NewDelay = d2.getInitialDelayAmt() * delayMultiplier;
    int d3NewDelay = d3.getInitialDelayAmt() * delayMultiplier;
    int d4NewDelay = d4.getInitialDelayAmt() * delayMultiplier;
    
    d1.setDelay (d1NewDelay); // TODO - remove +2 once above bug is fixed
    d2.setDelay (d2NewDelay);
    d3.setDelay (d3NewDelay);
    d4.setDelay (d4NewDelay);
}
